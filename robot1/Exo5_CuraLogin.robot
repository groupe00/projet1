*** Settings ***
Resource    Exo5_Squash_Ressource.resource

*** Test Cases ***
Test BDD 2
    [Setup]    Test Setup 

    Given L'utilisateur est sur la page d'accueil    
    When L'utilisateur souhaite prendre un rendez-vous
    And L'utilisateur se connecte    John Doe    ThisIsNotAPassword
    And L'utilisateur remplit le formulaire en choisissant le Healthcare Program    Medicare
    And L'utilisateur clique sur Book Appointment
    Then Le récapitulatif du rendez-vous apparait
    And Le récapitulatif correspond à ce qui a été saisi    Medicare

    [Teardown]    Test TearDown

Test BDD 3
    [Setup]    Test Setup 

    Given L'utilisateur est sur la page d'accueil    
    When L'utilisateur souhaite prendre un rendez-vous
    And L'utilisateur se connecte    John Doe    ThisIsNotAPassword
    And L'utilisateur remplit le formulaire en choisissant le Healthcare Program    Medicaid
    And L'utilisateur clique sur Book Appointment
    Then Le récapitulatif du rendez-vous apparait
    And Le récapitulatif correspond à ce qui a été saisi    Medicaid

    [Teardown]    Test TearDown


Test BDD 4
    [Setup]    Test Setup 

    Given L'utilisateur est sur la page d'accueil    
    When L'utilisateur souhaite prendre un rendez-vous
    And L'utilisateur se connecte    John Doe    ThisIsNotAPassword
    And L'utilisateur remplit le formulaire en choisissant le Healthcare Program    None
    And L'utilisateur clique sur Book Appointment
    Then Le récapitulatif du rendez-vous apparait
    And Le récapitulatif correspond à ce qui a été saisi    None

    [Teardown]    Test TearDown